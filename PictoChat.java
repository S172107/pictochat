import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.LinkedList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
public class PictoChat extends JFrame implements WindowListener{
	int width = 1050, height = 1000;
	int port;
	String name;
	String connectHost;
	String myHost;
	ServerSocket ssocket;
	LinkedList<String>  sskts;		//ServerSockets of connecting nodes
	LinkedList<PictWindow> pictWindows;	//
	LinkedList<Stream> objss;		//ObjectStreams that has to send
	Pict tmp_pict;
	PictWindow tmp_pictWindow;
	JPanel rightPanel;
	JFrame initJFrame;
	JScrollPane scrollPane;
	DrawPict drawPict;
	Canbus canbus;
	History history;
	Dialog dialog;

	public PictoChat(String _title, String _connectHost, int _port, String _name) throws IOException{
		super(_title);
		name = _name;
		connectHost = _connectHost;
		myHost = InetAddress.getLocalHost().getHostAddress();
		port = _port;
		tmp_pict = new Pict();
		tmp_pictWindow = new PictWindow(name, new LinkedList<Pict>());
		pictWindows = new LinkedList<PictWindow>();
		sskts = new LinkedList<String>();
		ssocket = new ServerSocket(port);
		sskts.add(InetAddress.getLocalHost().getHostAddress());
		objss = new LinkedList<Stream>();
		this.setBounds(new Rectangle(width, height));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		drawPict = new DrawPict(this);
		canbus = new Canbus(this);
		history = new History(this);
		dialog = new Dialog(this);
		Container pane = this.getContentPane();
		this.setLayout(new BoxLayout(pane, BoxLayout.X_AXIS));
		scrollPane  = new JScrollPane(history,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setPreferredSize(new Dimension(550,1000));
		pane.add(scrollPane);
		rightPanel.add(dialog);
		rightPanel.add(canbus);
		rightPanel.add(drawPict);
		pane.add(rightPanel);
		this.setResizable(false);
		this.setVisible(true);
		this.addWindowListener(this);
	}

	public static void main(String[] args) throws IOException {
		try {
			PictoChat pictoChat = new PictoChat("PictoChat", args[0], Integer.parseInt(args[1]), args[2]);
			new ServerManager(pictoChat).start();
			if(!pictoChat.connectHost.equals(pictoChat.myHost) &&
					!pictoChat.connectHost.equals("localhost")) {
				pictoChat.connect();
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("使い方: PictoChat IPアドレス ポート番号 ニックネーム");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	void connect() throws UnknownHostException, IOException, ClassNotFoundException {
		System.out.println("connect invoked");
		Stream objs = new Stream(new Socket(connectHost, port));
		objs.writeObject(new PacketData(PacketData.FIRST_CONNECT, null));
		PacketData data = (PacketData) objs.readObject();
		if(data.type == PacketData.FIRST_CONNECT_ACCEPTED) {
			this.sskts = (LinkedList<String>) data.obj;
		}
		objs.close();
		for(int i=0;i<this.sskts.size();i++) {
			Stream tmp = new Stream(new Socket(sskts.get(i), port));
			tmp.writeObject(new PacketData(PacketData.CONNECT, this.myHost));
			objss.add(tmp);
			this.dialog.appendDialog(sskts.get(i)+"に接続しました");
			new RecieveManager(this, tmp).start();
		}
		sskts.add(myHost);
	}

	void disconnect() throws IOException {
		System.out.println("disconnect invoked");
		sendAll(new PacketData(PacketData.DISCONNECT, myHost));
	}

	void disconnectRecieved(String _host) throws IOException {
		this.dialog.appendDialog(_host + "が切断しました");
		for(int i=0;i<sskts.size();i++) {
			if(sskts.get(i).equals(_host)) {
				sskts.remove(i);
				break;
			}
		}
		for(int i=0;i<objss.size();i++) {
			if(objss.get(i).getSocket().getInetAddress()
					.toString().equals("/"+_host)) {
				objss.get(i).close();
				objss.remove(i);
			}
		}
	}

	void dataSend() throws IOException {
		sendAll(new PacketData(PacketData.DATA, tmp_pictWindow));
		pictWindows.add(tmp_pictWindow.copy());
		if(pictWindows.size() > 10) {
			pictWindows.remove(0);
		}
		tmp_pictWindow.clear();
		canbus.clear();
		history.printHistory();
	}

	void dataRecieved(Object _input) {
		pictWindows.add(((PictWindow) _input).copy());
		if(pictWindows.size() > 10) {
			pictWindows.remove(0);
		}
		history.printHistory();
	}

	void sendAll(PacketData _data) throws IOException {
		for(int i=0;i<objss.size();i++) {
			objss.get(i).writeObject(_data);
		}
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		try {
			this.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			System.exit(0);
		}

	}

	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowClosed(WindowEvent arg0) {}



	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent e) {}

	@Override
	public void windowIconified(WindowEvent e) {}

	@Override
	public void windowOpened(WindowEvent e) {}
}


class ServerManager extends Thread{
	PictoChat target;
	ServerManager(PictoChat _target) throws IOException{
		target = _target;
	}
	@Override
	public void run() {
		try {
			target.dialog.appendDialog("あなたのIPアドレスは "+InetAddress.getLocalHost().getHostAddress()+"です");
			while(true) {
				Stream tmp = new Stream(target.ssocket.accept());
				PacketData data = (PacketData) tmp.readObject();
				if(data.type == PacketData.FIRST_CONNECT) {
					tmp.writeObject(new PacketData(PacketData.FIRST_CONNECT_ACCEPTED, target.sskts));
					tmp.close();
				}else if(data.type == PacketData.CONNECT) {
					target.sskts.add((String)data.obj);
					target.dialog.appendDialog(data.obj+"が接続しました");
					tmp.writeObject(new PacketData(PacketData.CONNECT_ACCEPTED, null));
					target.objss.add(tmp);
					new RecieveManager(target, tmp).start();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

class Stream{
	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;
	Stream(Socket _socket) throws IOException{
		socket = _socket;
		out = new PrintWriter(
				new BufferedWriter(
						new OutputStreamWriter(
								socket.getOutputStream())));
		in = new BufferedReader(
				new InputStreamReader(
						socket.getInputStream()));
		System.out.println("connect established:"+socket.toString());
	}
	PacketData readObject() throws IOException{
		String inputData = in.readLine();
		System.out.println("RECV: " + inputData);
		return PacketData.fromString(inputData);
	}
	void writeObject(PacketData _packet) throws IOException{
		System.out.println("SEND: " + _packet.toString());
		out.println(_packet.toString());
		out.flush();
	}
	Socket getSocket() {
		return socket;
	}
	void close() throws IOException{
		in.close();	out.close();	socket.close();
	}
}


class RecieveManager extends Thread {
	Socket socket;
	Stream objs;
	PacketData data;
	PictoChat target;
	RecieveManager(PictoChat _target,Stream _objs) throws IOException{
		target = _target; objs = _objs;

	}
	@Override
	public void run() {
		try {
			loop: while((data = (PacketData) objs.readObject()) != null) {
				switch(data.type) {
				case PacketData.DATA:
					target.dataRecieved(data.obj);
					break;
				case PacketData.DISCONNECT:
					target.disconnectRecieved((String) data.obj);
					break loop;
				default:
					break;
				}
			}
			objs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}




class PacketData implements Serializable{
	int type;
	final static int FIRST_CONNECT = 0;
	final static int FIRST_CONNECT_ACCEPTED = 1;
	final static int CONNECT = 2;
	final static int CONNECT_ACCEPTED = 3;
	final static int DATA = 4;
	final static int DISCONNECT = 5;
	final static int DISCONNECT_ACCEPTED = 6;
	Object obj;
	PacketData(int _type){
		type = _type;	obj = null;
	}
	PacketData(int _type, Object _obj){
		type = _type;	obj = _obj;
	}
	@Override
	public String toString() {
		if(obj == null) {
			return type + ": ";
		}else {
			if(obj instanceof LinkedList) {
				String result = type + ":";
				@SuppressWarnings("unchecked")
				LinkedList<String> data = (LinkedList<String>)obj;
				for(int i=0; i<data.size()-1; i++) {
					result += data.get(i) + ",";
				}
				result += data.get(data.size()-1);
				return result;
			}else {
				return type + ":" + obj.toString();
			}
		}
	}
	public static PacketData fromString(String _data) {
		String inputData = _data;
		String data_type = inputData.split(":")[0];
		String data = inputData.split(":")[1];
		Object obj;
		switch(Integer.parseInt(data_type)) {
		case PacketData.FIRST_CONNECT_ACCEPTED:
			LinkedList<String> tmp_obj = new LinkedList<String>();
			String[] data_elements = data.split(",");
			for(int i=0;i<data_elements.length;i++) {
				tmp_obj.add(data_elements[i]);
			}
			obj = tmp_obj;
			break;
		case PacketData.CONNECT:
		case PacketData.DISCONNECT:
			obj = data;
			break;
		case PacketData.DATA:
			obj = PictWindow.fromString(data);
			break;
		default:
			obj = null;
			break;
		}
		return new PacketData(
				Integer.parseInt(data_type),
				obj
				);
	}
}

@SuppressWarnings("unchecked")
class PictWindow implements Serializable{
	String send_by;
	LinkedList<Pict> windows;
	PictWindow(String _send_by, LinkedList<Pict> _windows){
		send_by = _send_by;	windows = _windows;
	}
	public PictWindow() {
		send_by = null; windows = new LinkedList<Pict>();
	}
	void draw(Graphics g) {
		g.drawString(send_by, 2, 10);
		g.drawRect(0, 0, 50, 12);
		for(int i=0;i<windows.size();i++) {
			windows.get(i).draw(g);
		}
	}
	void add(Pict _pict) {
		windows.add(_pict.copy());
	}
	void clear() {
		windows.clear();
	}
	PictWindow copy() {
		return new PictWindow(send_by,(LinkedList<Pict>) windows.clone());
	}
	void Erase(int X,int Y) {
        for(int i=0;i<windows.size();i++) {
            if(windows.get(i).CheckErase(X,Y)) {
                windows.remove(i);
            }
        }
    }
	@Override
	public String toString() {
		String result = new String(send_by + "@");
		for(int i=0; i<windows.size(); i++) {
			result = result + windows.get(i).toString() + ";";
		}
		return result;
	}
	public static PictWindow fromString(String _str) {
		String str_send_by = _str.split("@")[0];
		String str_windows = _str.split("@")[1];
		String[] str_window = str_windows.split(";");
		LinkedList<Pict> data_windows = new LinkedList<Pict>();
		for(int i=0; i<str_window.length; i++) {
			data_windows.add(Pict.fromString(str_window[i]));
		}
		return new PictWindow(str_send_by, data_windows);
	}
}

class Pict implements Serializable{
	char type;
	int start_x, start_y, end_x, end_y;
	String str;
	Color color;
	Pict(){
		type = 'l'; start_x = start_y = end_x = end_y = 0;
		str = null; color = Color.BLACK;
	}
	Pict(char _type, int _start_x, int _start_y, int _end_x, int _end_y, String _str, Color _color){
		type = _type; start_x = _start_x; start_y = _start_y; end_x = _end_x;
		end_y = _end_y; str = _str; color = _color;
	}
	void draw(Graphics g) {
		g.setColor(color);
		int x = Math.min(start_x, end_x);
		int y = Math.min(start_y, end_y);
		int width = Math.abs(start_x - end_x);
		int height = Math.abs(start_y - end_y);
		switch(type) {
		case 'l':
			g.drawLine(start_x, start_y, end_x, end_y);	break;
		case 'o':
			g.drawOval(x, y, width, height);	break;
		case 'O':
			g.fillOval(x, y, width, height);	break;
		case 'r':
			g.drawRect(x, y, width, height);	break;
		case 'R':
			g.fillRect(x, y, width, height);	break;
		/*  とりあえず放置
		    case 's':
			g.drawString(str, x, y);			break;*/
		}

	}
	Pict copy() {
		return new Pict(this.type,this.start_x,this.start_y,this.end_x,this.end_y,this.str,this.color);
	}
	void clear() {
		type = 'l'; start_x = start_y = end_x = end_y = 0;
		str = null; color = Color.BLACK;
	}
	boolean CheckErase(int X,int Y) {
        if(start_x <= X && end_x >= X && start_y <= Y && end_y >= Y) {
            return true;
        }
        else {
            return false;
        }
    }
	@Override
	public String toString() {
		return type + "," +
				String.valueOf(start_x) + "," +
				String.valueOf(start_y) + "," +
				String.valueOf(end_x) + "," +
				String.valueOf(end_y) + "," +
				str + "," +
				String.valueOf(color.getRGB())
				;
	}
	public static Pict fromString(String _str) {
		String[] splt_str = _str.split(",");
		return new Pict(
				splt_str[0].toCharArray()[0],
				Integer.parseInt(splt_str[1]),
				Integer.parseInt(splt_str[2]),
				Integer.parseInt(splt_str[3]),
				Integer.parseInt(splt_str[4]),
				splt_str[5],
				new Color(Integer.parseInt(splt_str[6]))
				);
	}
}

class Dialog extends JPanel{
	PictoChat pictoChat;
	JTextArea textarea;
	JScrollPane scrollPane;
	Dialog (PictoChat _pictoChat){
		pictoChat = _pictoChat;
		this.setPreferredSize(new Dimension(500,500));
		this.setBorder(new LineBorder(Color.YELLOW,1,true));
		textarea = new JTextArea();
		scrollPane = new JScrollPane(textarea,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setPreferredSize(new Dimension(500,470));
		textarea.setEditable(false);
		this.add(scrollPane);
	}

	void appendDialog(String _str) {
		textarea.append(_str+"\n");
	}

}
class History extends JPanel{
	PictoChat pictoChat;
	History(PictoChat _pictoChat){
		pictoChat = _pictoChat;
		this.setPreferredSize(new Dimension(520,2600));
		this.setBorder(new LineBorder(Color.GREEN, 1, true));
	}
	void printHistory() {
		this.removeAll();
		for(int i=0; i<pictoChat.pictWindows.size(); i++) {
			this.add(new HistoryPanel(pictoChat.pictWindows.get(i)));
		}
		this.validate();
		pictoChat.scrollPane.setLayout(null);
	}
	class HistoryPanel extends JPanel{
		PictWindow pictWindow;
		HistoryPanel(PictWindow _pictWindow){
			pictWindow = _pictWindow;
			this.setPreferredSize(new Dimension(500,250));
			this.setBorder(new LineBorder(Color.BLACK,1,true));
		}
		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			pictWindow.draw(g);
		}
	}
}

class Canbus extends JPanel implements MouseListener, MouseMotionListener{
	PictoChat pictoChat;
	int start_x, start_y, end_x, end_y;
	Canbus(PictoChat _pictoChat){
		pictoChat = _pictoChat;
		this.setPreferredSize(new Dimension(500,250));
		this.setBorder(new LineBorder(Color.RED, 1, true));
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		//Dimension dim=getSize();
		//g.setColor(getBackground());
		//g.fillRect(0,0,dim.width,dim.height);
		pictoChat.tmp_pictWindow.draw(g);
		pictoChat.tmp_pict.start_x = start_x;
		pictoChat.tmp_pict.start_y = start_y;
		pictoChat.tmp_pict.end_x = end_x;
		pictoChat.tmp_pict.end_y = end_y;
		pictoChat.tmp_pict.draw(g);
	}
	@Override
	public void mousePressed(MouseEvent e) {
		start_x = e.getX(); start_y = e.getY();
		if(pictoChat.tmp_pict.type == 'e') {
            pictoChat.tmp_pictWindow.Erase(start_x,start_y);
            this.repaint();
        }
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		if(pictoChat.tmp_pict.type != 'e') {
			pictoChat.tmp_pictWindow.add(pictoChat.tmp_pict);
		}
	}
	@Override
	public void mouseDragged(MouseEvent e) {
		end_x = e.getX(); end_y = e.getY();
		this.repaint();
	}
	@Override
	public void mouseMoved(MouseEvent arg0) {}
	void clear() {
		start_x = start_y = end_x = end_y = 0;
		this.repaint();
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {}
	@Override
	public void mouseEntered(MouseEvent arg0) {}
	@Override
	public void mouseExited(MouseEvent arg0) {}
}

//
class DrawPict extends JPanel implements ActionListener{
	JMenu menu;
	JButton Line,Blush,NonFillSquare,FillSquare,NonFillCircle,FillCircle,Erase;
	JButton Send, Clear;
	PictoChat pictoChat;
	GridBagLayout gbl;
	Canbus canbus;
	int start_x, start_y;
	char type;

	DrawPict(PictoChat _pictoChat){
		pictoChat = _pictoChat;
		gbl = new GridBagLayout();
		this.setPreferredSize(new Dimension(500,250));
		InitGUI();
	}

	void addButton(JButton b, int x, int y, int w, int h) {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = w;
		gbc.gridheight = h;
		gbl.setConstraints(b, gbc);
		this.add(b);
	}

	void InitGUI(){
		this.setLayout(gbl);
		this.setBorder(new LineBorder(Color.BLUE, 1, true));
		Line = new JButton("線");
		Line.addActionListener(this);
		NonFillSquare = new JButton("塗りつぶし無し四角");
		NonFillSquare.addActionListener(this);
		FillSquare = new JButton("塗りつぶし四角");
		FillSquare.addActionListener(this);
		NonFillCircle = new JButton("塗りつぶし無し円");
		NonFillCircle.addActionListener(this);
		FillCircle = new JButton("塗りつぶし円");
		FillCircle.addActionListener(this);
		Clear = new JButton("画面クリア");
		Clear.addActionListener(this);
		Erase = new JButton("消しゴム");
	    Erase.addActionListener(this);
		Send = new JButton("送信");
		Send.addActionListener(this);
		addButton(Line, 0,1,1,2);
		addButton(NonFillSquare,2,0,2,2);
		addButton(FillSquare,2,2,2,2);
		addButton(NonFillCircle,4,0,2,2);
		addButton(FillCircle,4,2,2,2);
		addButton(Erase,2,4,2,1);
		addButton(Send,0,5,6,1);
		addButton(Clear,5,4,1,1);
	}
	@Override
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == Line) {
			pictoChat.tmp_pict.type = 'l';
		}
		else if(e.getSource() == Blush) {
			pictoChat.tmp_pict.type = 'b';
		}
		else if(e.getSource() == NonFillSquare) {
			pictoChat.tmp_pict.type = 'r';
		}
		else if(e.getSource() == FillSquare) {
			pictoChat.tmp_pict.type = 'R';
		}
		if(e.getSource() == NonFillCircle) {
			pictoChat.tmp_pict.type = 'o';
		}
		else if(e.getSource() == FillCircle) {
			pictoChat.tmp_pict.type = 'O';
		}
		else if(e.getSource() == Erase) {
			pictoChat.tmp_pict.type = 'e';
		}
		else if(e.getSource() == Clear) {
			pictoChat.tmp_pictWindow.clear();
			pictoChat.canbus.clear();
		}
		else if(e.getSource() == Send) {
			try {
				pictoChat.dataSend();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
}

